# IVT object localization

### Step 1: Open the package ivt_armarx.

Note that the path is different in your case compared to the image. 

![](images/openPackage.png)

### Step 2: Create two scenarios with the packages shown in the subsequent figure. 

The scenario ivt_test is for the object localization, the ArMemObjectMemory is necessary to write object poses into the memory. 


![](images/allScenarios.png)

### Step 3: Modification of object_localizer properties.

Click on object_localizer and and set the properties .ProviderName and .visu.enabled. 

![](images/clickObjectLocalizer.png)


![](images/localizerProperties.png)


### Step 4: Set the devide ID.

Click on RCImageprovider and change the property .deviceID to the MAC address of your Roboception

![](images/clickRCImageprovider.png)


![](images/setDeviceId.png)


### Step 5: Check the object files.

The object localization with ivt is possible if objects have a <objectname>.dat and <objectname>.mat file. A file <objectname>.transform.json is optional. The name of the dataset does not matter in this case. For some objects there might be a .iv file instead of a .wrl file. 
The .dat and .mat file are created by FeatureLearning in the ArmarX GUI. The .transform.json file can be created if a transformation of the object model, but not the object pose, is needed.


![](images/objectFiles.png)

### Step 6: Change image provider.

Open the VisionX.ImageMonitor and click on the wrench-Symbol to change its properties. In Proxy for Image Source choos ivt\_object\_localizerResult.

![](images/changeImageProvider.png)

### Step 7: Request objects.

Open a RemoteGUI. All objects, that include a .dat file, are  possible to request. The objects are sorted by their datasets. If you request an object and it is seen by the Roboception then the pose is written into the memory. 

![](images/requestObjects.png)

![](images/localizeCorny.png)


### Step 8: Visualization of the object pose.
Open ArViz. The object pose is visualized. If it is missing, right-click on the white area inside the Visualization.3D Viewer and choose 'View all'. You can also check in ArViz that the visualization of your object is enabled. 

![](images/poseCorny.png)

![](images/arvizInfo.png)

### Step 9: Get the object poses.

Open ArMem.MemoryViewer. Click Update and optionally enable the checkbox 'auto update'. If you requested an object its pose is written here including its particular timestamp.

![](images/posesMemory.png)

![](images/posesMemory2.png)


