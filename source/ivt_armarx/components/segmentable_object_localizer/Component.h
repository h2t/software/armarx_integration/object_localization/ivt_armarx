/** @file Component.h
 *
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SimTrackArmarX::ArmarXObjects::sim_track
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <mutex>

#include <Eigen/SVD>

#include <SimoxUtility/json.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/RequestedObjects.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <VisionX/core/ImageBuffer.h>
#include <VisionX/core/ImageProcessor.h>

#include "IvtObjects.h"
#include <SegmentableRecognition/SegmentableRecognition.h>
#include <TexturedRecognition/TexturedObjectDatabase.h>
#include <TexturedRecognition/TexturedRecognition.h>
#include <ivt_armarx/components/segmentable_object_localizer/ComponentInterface.h>


namespace ivt_armarx::components::segmentable_object_localizer
{

    class Component :
        virtual public ComponentInterface,
        virtual public visionx::ImageProcessor,
        virtual public armarx::ObjectPoseProviderPluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
    public:
        using Transformation = Eigen::Isometry3f;


        Component();

        // See armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        static std::string GetDefaultName();

        bool updateObjects(const std::vector<armarx::ObjectID>& added,
                           const std::vector<armarx::ObjectID>& released);


    public:
    protected:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
        void onInitImageProcessor() override;

        void onConnectImageProcessor() override;

        void onDisconnectImageProcessor() override;

        void onExitImageProcessor() override;

        void process() override;

        void ivtCalibration();

        void ivtCreateObjectDatabase();

        void ivtInitializeCameraParameters();

        // ObjectPoseProvider interface
        armarx::objpose::ProviderInfo
        getProviderInfo(const Ice::Current& = Ice::emptyCurrent) override;

        armarx::objpose::provider::RequestObjectsOutput
        requestObjects(const armarx::objpose::provider::RequestObjectsInput& input,
                       const Ice::Current& = Ice::emptyCurrent) override;

        armarx::objpose::ProvidedObjectPoseSeq
        makeProvidedObjectPoses(const std::vector<Object3DEntry>& objectList,
                                const armarx::core::time::DateTime& timestamp);

        // Remote Gui
        void createRemoteGuiTab();

        void RemoteGui_update() override;


        std::optional<Transformation>
        createTransformationMatrix(std::filesystem::path pathToTransformJson);

        Eigen::Matrix3f flipOrientationXYPlane(const Eigen::Matrix3f& orientation);

    protected:
        void visualizeObjectPoses(std::vector<armarx::objpose::ProvidedObjectPose> objectPoses);

    private: // parameters
        struct Properties
        {
            std::string providerName;
            std::vector<std::string> initialObjectIDs;
            bool visuEnabled = false;
            bool texturedRecognitionVerbose = false;
            std::string colorParameterSetFile = "colors.txt";
        } properties;


    private:
        std::vector<armarx::ObjectInfo> supportedObjects;
        armarx::objpose::RequestedObjects requestedObjects;

        bool isObjectSupported(const armarx::data::ObjectID& id) const;

        static const std::string defaultName;

        visionx::ImageProviderInfo imageProviderInfo;
        armarx::MetaInfoSizeBasePtr imageMetaInfo;
        visionx::ImageBuffer images;
        visionx::ImageBuffer ppResultImages;

        std::map<std::string, std::vector<armarx::ObjectInfo>> supportedObjectsByDataset;
        std::mutex requestedObjectsMutex;
        std::unique_ptr<IvtObjects> ivtObjects;

        // using SystemModelT = SystemModelSE3<double>;
        // std::map<armarx::ObjectID, UnscentedKalmanFilter<SystemModelT>> ukfs;

        std::mutex mutex;

        std::unique_ptr<CTexturedRecognition> texturedRecognition;
        std::map<std::string, Transformation> classTransformation;

        std::unique_ptr<CSegmentableRecognition> segmentableRecognition;

        std::string referenceFrameName;
        armarx::viz::StagedCommit stage;


    private: // gui
        struct Tab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::ComboBox objectDatasetComboBox;
            armarx::RemoteGui::Client::ComboBox objectNameComboBox;
            armarx::RemoteGui::Client::Button requestObjectButton;

            std::map<armarx::ObjectID, armarx::RemoteGui::Client::Button> releaseObjectButtons;

            armarx::RemoteGui::Client::LineEdit propertyLine;
        };

        Tab tab;
    };

} // namespace ivt_armarx::components::segmentable_object_localizer
