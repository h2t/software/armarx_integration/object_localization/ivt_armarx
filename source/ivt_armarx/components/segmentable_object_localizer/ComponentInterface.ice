/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    ivtarmarx::object_localizer
 * author     Fabian Reister ( fabian dot reister at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
*/


#pragma once


#include <RobotAPI/interface/objectpose/ObjectPoseProvider.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>


module ivt_armarx { module components { module segmentable_object_localizer
{
    interface ComponentInterface extends
        armarx::objpose::ObjectPoseProvider,
        visionx::ImageProcessorInterface
    {
    };

};};};
