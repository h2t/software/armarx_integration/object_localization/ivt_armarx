#pragma once

#include <vector>

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <VisionX/core/ImageBuffer.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/Calibration.h>

namespace ivt_armarx::components::segmentable_object_localizer
{
    class IvtObjects : armarx::Logging
    {
    public:
        IvtObjects();
        ~IvtObjects() override;


        void setObjects(const std::vector<armarx::ObjectID>& objectIDs);
        bool addObjects(const std::vector<armarx::ObjectID>& objectIDs);
        bool releaseObjects(const std::vector<armarx::ObjectID>& objectIDs);
        bool updateObjects(const std::vector<armarx::ObjectID>& added,
                           const std::vector<armarx::ObjectID>& released);

        std::vector<armarx::ObjectInfo> getSupportedObjects() const;
        std::vector<armarx::ObjectInfo>
        getRequestedObjects(std::vector<std::string> requestedNames) const;

        armarx::ObjectFinder objectFinder;

        std::map<armarx::ObjectID, armarx::ObjectInfo> requestedObjectInfos;
        std::set<armarx::ObjectID> requestedObjectIDs;

        std::mutex objectPosesMutex;
    };

} // namespace ivt_armarx::components::segmentable_object_localizer
