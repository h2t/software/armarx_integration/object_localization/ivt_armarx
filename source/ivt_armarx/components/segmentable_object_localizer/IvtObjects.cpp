#include "IvtObjects.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <VisionX/tools/opencv_conversions.h>

namespace ivt_armarx::components::segmentable_object_localizer
{
    IvtObjects::IvtObjects()
    {
        armarx::Logging::setTag("IVT");
    }

    IvtObjects::~IvtObjects() = default;

    void
    IvtObjects::setObjects(const std::vector<armarx::ObjectID>& objectIDs)
    {
        requestedObjectInfos.clear();
        addObjects(objectIDs);
    }

    bool
    IvtObjects::addObjects(const std::vector<armarx::ObjectID>& objectIDs)
    {
        return updateObjects(objectIDs, {});
    }

    bool
    IvtObjects::releaseObjects(const std::vector<armarx::ObjectID>& objectIDs)
    {
        return updateObjects({}, objectIDs);
    }

    bool
    IvtObjects::updateObjects(const std::vector<armarx::ObjectID>& added,
                              const std::vector<armarx::ObjectID>& released)
    {
        if (added.empty() && released.empty())
        {
            return false;
        }

        bool changed = false;

        for (const armarx::ObjectID& id : released)
        {
            changed |= requestedObjectInfos.erase(id) > 0;
        }

        for (const auto& id : added)
        {
            if (requestedObjectInfos.count(id) == 0)
            {
                if (std::optional<armarx::ObjectInfo> info = objectFinder.findObject(id))
                {
                    armarx::ObjectInfo siftinfo(*info);
                    requestedObjectInfos.emplace(id, siftinfo);
                    changed = true;
                }
            }
        }

        return changed;
    }

    std::vector<armarx::ObjectInfo>
    IvtObjects::getSupportedObjects() const
    {
        bool checkPaths = false;
        std::vector<armarx::ObjectInfo> supportedObjects;
        std::vector<armarx::ObjectInfo> objects = objectFinder.findAllObjects(checkPaths);

        for (const armarx::ObjectInfo& object : objects)
        {
            if (std::filesystem::is_regular_file(object.file("dat").absolutePath))
            {
                supportedObjects.push_back(object);
            }

            if (object.className() == "green-cup")
            {
                supportedObjects.push_back(object);
            }
        }


        return supportedObjects;
    }

    std::vector<armarx::ObjectInfo>
    IvtObjects::getRequestedObjects(std::vector<std::string> requestedNames) const
    {
        std::vector<armarx::ObjectInfo> supportedObjects = IvtObjects::getSupportedObjects();
        std::vector<armarx::ObjectInfo> requestedObjects;
        if (requestedObjects.empty())
        {

            return supportedObjects;
        }

        for (auto& supportedObject : supportedObjects)
        {
            for (auto& requestedName : requestedNames)
            {
                if (requestedName == supportedObject.className())
                {
                    requestedObjects.push_back(supportedObject);
                }
            }
        }

        return requestedObjects;
    }

} // namespace ivt_armarx::components::segmentable_object_localizer
