﻿/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SimTrackArmarX::ArmarXObjects::sim_track
 * @author     Felizia Quetscher ( felizia dot quetscher at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Component.h"

#include "ArmarXCore/core/exceptions/LocalException.h"
#include "ArmarXCore/core/logging/Logging.h"
#include "ArmarXCore/core/time/Clock.h"
#include "ArmarXCore/core/time/DateTime.h"
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/json.h>
#include <SimoxUtility/shapes/OrientedBox.h>
#include <SimoxUtility/math/pose.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/ArmarXObjects/ProvidedObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/RequestedObjects.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/opencv_conversions.h>
#include <VisionX/tools/TypeMapping.h>

#include <Calibration/StereoCalibration.h>

#include <filesystem>

using namespace armarx;

namespace visionx
{
    const std::string
    IVTObjectLocalizer::defaultName = "ivt_object_localizer";

    std::string
    IVTObjectLocalizer::getDefaultName() const
    {
        return IVTObjectLocalizer::defaultName;
    }

    std::string
    IVTObjectLocalizer::GetDefaultName()
    {
        return IVTObjectLocalizer::defaultName;
    }

    IVTObjectLocalizer::IVTObjectLocalizer() :
        ivtObjects(std::make_unique<armarx::IvtObjects>())
    {                
    }

    armarx::PropertyDefinitionsPtr
    IVTObjectLocalizer::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr
                defs = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        defs->optional(providerName, "ProviderName", "Image provider name.");
        defs->optional(initialObjectIDs, "Objects", "Object IDs of objects to be tracked.")
        .map("Kitchen/vitalis-cereal, Kitchen/vanilla-cookies", {});

        defs->optional(visuEnabled, "visu.enabled", "True to enable visualization of tracked objects.");
        defs->optional(texturedRecognitionVerbose, "texturedRecognitionVerbose", "");

        return defs;
    }


    void IVTObjectLocalizer::onInitImageProcessor()
    {
        ARMARX_TRACE;

        if (providerName.empty())
        {
            ARMARX_WARNING << "Empty provider name.";
            return;
        }

        usingImageProvider(providerName);

        supportedObjects = ivtObjects->getSupportedObjects();

        std::stringstream  stringstream;
        for (const armarx::ObjectInfo& object : supportedObjects)
        {
            supportedObjectsByDataset[object.dataset()].push_back(object);
            stringstream << " " << object << "\n";
        }
        ARMARX_INFO << "Supported objects:\n" << stringstream.str();

        // Check if objects have a transformation file
        ObjectFinder objectFinder;
        std::vector<ObjectInfo> objects = objectFinder.findAllObjects(false);
        const std::string fileSuffix = "transform.json";

        ARMARX_INFO << "Searching for transformations (transform.json)";
        for (const ObjectInfo& object : objects)
        {            
            std::filesystem::path pathToTransformJson = object.file(fileSuffix).absolutePath;
            if (std::filesystem::is_regular_file(pathToTransformJson))
            {
                ARMARX_INFO << "Found transformation for object class `" << object.idStr() << "`: " << pathToTransformJson;
                if(const std::optional<Transformation> transformation = createTransformationMatrix(pathToTransformJson))
                {
                    classTransformation[object.idStr()] = transformation.value();
                }
            }
        }
    }

    void IVTObjectLocalizer::ivtCalibration()
    {
        float width = imageProviderInfo.imageFormat.dimension.width;
        float height = imageProviderInfo.imageFormat.dimension.height;
        float threshold = 0.1;

        texturedRecognition = new CTexturedRecognition(width, height, threshold);
        texturedRecognition->SetVerbose(texturedRecognitionVerbose);
        texturedRecognition->SetStereo(true);
        texturedRecognition->SetQualityThreshold(0.0001);
        texturedRecognition->SetRecognitionThresholds(10, 5.0);
        Eigen::Vector3f minPoint(-3000.0f, -3000.0f, 100.0f);
        Eigen::Vector3f maxPoint(3000.0f, 3000.0f, 3500.0f);

        Vec3d validResultBoundingBoxMin, validResultBoundingBoxMax;
        Math3d::SetVec(validResultBoundingBoxMin, minPoint.x(), minPoint.y(), minPoint.z());
        Math3d::SetVec(validResultBoundingBoxMax, maxPoint.x(), maxPoint.y(), maxPoint.z());

        texturedRecognition->GetObjectDatabase()->SetCorrelationParameters(19, minPoint.z(), maxPoint.z(), 0.7f);
    }

    void IVTObjectLocalizer::ivtCreateObjectDatabase()
    {
        texturedRecognition->GetObjectDatabase()->ClearDatabase();

        ARMARX_INFO << "Localized objects size: "
                    << texturedRecognition->GetObjectDatabase()->m_objectList.size();

        for (armarx::ObjectInfo& supportedObject : supportedObjects)
        {
            bool objectRequested = (ivtObjects->requestedObjectIDs).find(supportedObject.id())
                    != (ivtObjects->requestedObjectIDs).end();

            if (objectRequested)
            {
                std::filesystem::path absPathObject = supportedObject.file("dat").absolutePath;
                texturedRecognition->GetObjectDatabase()->AddClass(supportedObject.className(), absPathObject.u8string());
            }
        }
    }

    void IVTObjectLocalizer::ivtInitializeCameraParameters()
    {
        ImageProviderInterfacePrx imageProviderPrx = getProxy<ImageProviderInterfacePrx>(providerName);
        StereoCalibrationInterfacePrx calibrationProvider = StereoCalibrationInterfacePrx::checkedCast(imageProviderPrx);

        if (calibrationProvider)
        {
            std::unique_ptr<CStereoCalibration> stereoCalibration;
            stereoCalibration.reset(visionx::tools::convert(calibrationProvider->getStereoCalibration()));
            referenceFrameName = calibrationProvider->getReferenceFrame();
            texturedRecognition->GetObjectDatabase()->InitCameraParameters(stereoCalibration.get(), true);
        }
        else
        {
            ARMARX_ERROR << "unable to retrieve calibration.";
        }

    }

    void IVTObjectLocalizer::onConnectImageProcessor()
    {
        ARMARX_TRACE;

        imageProviderInfo = getImageProvider(providerName);
        images.allocate(imageProviderInfo);

        ARMARX_INFO << "Number of initial object ids: " << initialObjectIDs.size();
        for(const auto& id : initialObjectIDs)
        {
            ARMARX_INFO << "Requested `" << id << "`";
            if (ivtObjects->addObjects({id}))
            {
                ivtObjects->requestedObjectIDs.insert(id);
            }
        }



        createRemoteGuiTab();
        RemoteGui_startRunningTask();

        if (visuEnabled)
        {
            arviz.commitLayerContaining("Origin", armarx::viz::Pose("Origin").scale(1));
        }

        ivtCalibration();
        ivtCreateObjectDatabase();
        ivtInitializeCameraParameters();

        enableResultImages(imageProviderInfo.numberImages, imageProviderInfo.imageFormat.dimension,
                           visionx::ImageType::eRgb);
    }


    void IVTObjectLocalizer::onDisconnectImageProcessor()
    {
    }


    void IVTObjectLocalizer::onExitImageProcessor()
    {
    }


    std::optional<IVTObjectLocalizer::Transformation> IVTObjectLocalizer::createTransformationMatrix(std::filesystem::path json_path)
    {
        try
        {
            std::ifstream ifs(json_path);
            const nlohmann::basic_json json = nlohmann::json::parse(ifs);

            const Eigen::Vector3f ea = json.at("orientation").get<Eigen::Vector3f>();
            const Eigen::Vector3f t = json.at("translation").get<Eigen::Vector3f>();

            Eigen::Matrix3f n;
            n = Eigen::AngleAxisf(ea[2], Eigen::Vector3f::UnitZ())
                * Eigen::AngleAxisf(ea[1], Eigen::Vector3f::UnitY())
                * Eigen::AngleAxisf(ea[0], Eigen::Vector3f::UnitX());
            
            Transformation m = Transformation::Identity();
            m.translation() = t;
            m.linear() = n;

            return m;
        }
        catch(...)
        {
            ARMARX_WARNING << "Could not read transformation `" << json_path << "`. Reason: " << GetHandledExceptionString();
            return std::nullopt;
        }

    }



    void IVTObjectLocalizer::visualizeObjectPoses(std::vector<objpose::ProvidedObjectPose>  objectPoses)
    {
        armarx::viz::Layer poseLayer = arviz.layer("Pose Visualization");
        armarx::viz::Layer objectLayer = arviz.layer("Object Visualization");

        for (objpose::ProvidedObjectPose& objectPose : objectPoses)
		{
            const std::string objectIdName = objectPose.objectID.str();
            const Eigen::Matrix4f& pose = objectPose.objectPose;

            // remove the /0 instance suffix
		    std::string objectName = objectIdName;
            objectName.erase(objectName.length()-2); 
            viz::Pose vizPose = viz::Pose("Pose of " + objectIdName)
                    .scale(1.0f)
                    .pose(pose);
            poseLayer.add(vizPose);

            viz::Object vizObject = viz::Object(objectIdName).fileByObjectFinder(objectIdName).pose(pose);
            objectLayer.add(vizObject);
        }
        stage.add(poseLayer);
        stage.add(objectLayer);
        viz::CommitResult result = arviz.commit(stage);

    }



    Eigen::Matrix3f IVTObjectLocalizer::flipOrientationXYPlane(const Eigen::Matrix3f& orientation)
    {
        Eigen::Matrix3f flippedOrientation = orientation;
        flippedOrientation(0,0) *= (-1);
        flippedOrientation(1,0) *= (-1);
        flippedOrientation(2,0) *= (-1);
        flippedOrientation(0,2) *= (-1);
        flippedOrientation(1,2) *= (-1);
        flippedOrientation(2,2) *= (-1);

        return flippedOrientation;
    }

    armarx::objpose::ProvidedObjectPoseSeq
    IVTObjectLocalizer::makeProvidedObjectPoses(const std::vector<Object3DEntry>& objectList, const armarx::core::time::DateTime& timestamp)
    {
        ARMARX_TRACE;

        objpose::ProvidedObjectPoseSeq objectPoses;
        Transformation pose = Transformation::Identity();
        for (uint i = 0; i < objectList.size(); i++)
        {
            // Create className and dataset
            std::string ivFilePath = texturedRecognition->GetObjectDatabase()->m_objectList[i].sOivFilePath;
            std::filesystem::path path(ivFilePath);
            std::string className = path.parent_path().filename().string();
            std::string datasetName = path.parent_path().parent_path().filename().string();
            
            // we assume, that there is only one instance of a class
            const armarx::ObjectID objectId(datasetName, className, "0");

            if(!objectList[i].localizationValid)
            {
                ARMARX_INFO << "Object \'" << className << "\' not valid";
                continue;
            }

            Eigen::Vector3f position;
            position << objectList[i].pose.translation.x,
                        objectList[i].pose.translation.y,
                        objectList[i].pose.translation.z;

            Eigen::Matrix3f orientation;
            orientation << objectList[i].pose.rotation.r1,
                           objectList[i].pose.rotation.r2,
                           objectList[i].pose.rotation.r3,
                           objectList[i].pose.rotation.r4,
                           objectList[i].pose.rotation.r5,
                           objectList[i].pose.rotation.r6,
                           objectList[i].pose.rotation.r7,
                           objectList[i].pose.rotation.r8,
                           objectList[i].pose.rotation.r9;


            if (orientation(0,0) < 0)
            {
                orientation = flipOrientationXYPlane(orientation);
            }

            pose = Transformation(simox::math::pose(position, orientation));


            // remove the /0 instance suffix
		    std::string objectName = datasetName + "/" + className;
            if(classTransformation.count(objectName))
            {
                pose = pose * classTransformation.at(objectName);
            }



            std::scoped_lock lock(ivtObjects->objectPosesMutex);
            armarx::objpose::ProvidedObjectPose& objectPose = objectPoses.emplace_back();

            objectPose.providerName = getName();
            objectPose.objectType = objpose::ObjectType::KnownObject;
            objectPose.objectID = objectId;
            objectPose.objectPose = pose.matrix();
            objectPose.objectPoseFrame = referenceFrameName;
            objectPose.confidence = 1.0;
            objectPose.timestamp = timestamp;

            ARMARX_VERBOSE << "Localized: " << className;
        }

        if (visuEnabled)
        {
            visualizeObjectPoses(objectPoses);
        }

        return objectPoses;
    }

    void IVTObjectLocalizer::process()
    {
        ARMARX_TRACE;

        if (!waitForImages(1000))
        {
            ARMARX_WARNING << deactivateSpam(300) << "Timeout or error while waiting for image data";
            return;
        }

        if (ivtObjects->requestedObjectIDs.empty())
        {
            ARMARX_INFO << deactivateSpam(60) << "No object requested";
            return;
        }

        armarx::MetaInfoSizeBasePtr info;
        getImages(providerName, images.images, info);
       
        // just a guess, whether the timestamp is in µs
        const armarx::core::time::DateTime timestamp(armarx::core::time::Duration::MicroSeconds(info->timeProvided));

        std::scoped_lock lock(requestedObjectsMutex);

        texturedRecognition->DoRecognition(images.images, images.images, true, 50, true);

        // Convert localization to pose (contains orientation, pose.rotation, pose.translation)
        const std::vector<Object3DEntry> & objectList = texturedRecognition->GetObject3DList();

        ARMARX_INFO  << deactivateSpam(1) << "Number of localized objects: " << objectList.size() << " objects.";

        // Convert poses to Eigen::Matrix
        armarx::objpose::ProvidedObjectPoseSeq objectPoses = IVTObjectLocalizer::makeProvidedObjectPoses(objectList, timestamp);

        if (!ivtObjects->requestedObjectIDs.empty())
        {
            // Store object poses in new memory
            for (const auto& ob : objectPoses)
            {
                ARMARX_VERBOSE << "Name for pose: " << ob.objectID;
            }
            armarx::objpose::data::ProvidedObjectPoseSeq iceObjPoseSeq = armarx::objpose::toIce(objectPoses);
            objectPoseTopic->reportObjectPoses(getName(), armarx::objpose::toIce(objectPoses));
        }

        provideResultImages(images.images, info);
    }

    

    objpose::ProviderInfo IVTObjectLocalizer::getProviderInfo(const Ice::Current&)
    {
        objpose::ProviderInfo info;
        info.objectType = objpose::ObjectType::KnownObject;

        for (const auto& [dataset, objects] : supportedObjectsByDataset)
        {
            for (const ObjectInfo& object : objects)
            {
                info.supportedObjects.push_back(toIce(object.id()));
            }
        }

        return info;
    }

    objpose::provider::RequestObjectsOutput IVTObjectLocalizer::requestObjects(
        const objpose::provider::RequestObjectsInput& input, const Ice::Current&)
    {
        {
            std::scoped_lock lock(requestedObjectsMutex);
            requestedObjects.requestObjects(input.objectIDs, input.relativeTimeoutMS);
        }

        objpose::provider::RequestObjectsOutput output;

        for (const armarx::data::ObjectID& objectID : input.objectIDs)
        {
            output.results[objectID].success = isObjectSupported(objectID);
        }

        return output;
    }

    bool IVTObjectLocalizer::isObjectSupported(const data::ObjectID& id) const
    {
        auto it = supportedObjectsByDataset.find(id.dataset);
        if (it != supportedObjectsByDataset.end())
        {
            auto& infos = it->second;
            auto pred = [&id](const ObjectInfo & info)
            {
                return info.id().className() == id.className;
            };

            return std::find_if(infos.begin(), infos.end(), pred) != infos.end();
        }

        return false;
    }

    void IVTObjectLocalizer::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        GridLayout grid;
        int row = 0;
        if (!RemoteGui_isRunning())
        {
            std::vector<std::string> datasets = simox::alg::get_keys(supportedObjectsByDataset);
            std::vector<std::string> objects;


	    if (datasets.size() > 0)
	    {
		    for (armarx::ObjectInfo info : supportedObjectsByDataset.at(datasets.at(0)))
		    {
			objects.push_back(info.className());
		    }
	    }
	    else
	    {
		    ARMARX_WARNING << "No dataset available.";
		    return;
	    }
	    tab.objectDatasetComboBox.setOptions(datasets);
	    tab.objectNameComboBox.setOptions(objects);
        }

        else if (tab.objectDatasetComboBox.hasValueChanged())
        {
            std::string dataset = tab.objectDatasetComboBox.getValue();
            std::vector<std::string> objects;
            for (ObjectInfo& info : supportedObjectsByDataset.at(dataset))
            {
                objects.push_back(info.className());
            }
            tab.objectNameComboBox.setOptions(objects);
            tab.objectDatasetComboBox.setValue(dataset);
        }

        grid.add(Label("Dataset"), {row, 0}).add(Label("Class Name"), {row, 1});
        row++;

        grid.add(tab.objectDatasetComboBox, {row, 0}).add(tab.objectNameComboBox, {row, 1});
        row++;

        tab.requestObjectButton.setLabel("Request object");
        grid.add(tab.requestObjectButton, {row, 0}, {1, 2});
        row++;

        GroupBox requestedGroup;        {
            GridLayout requestedGrid;
            row = 0;

            // Add requested objects to RemoteGUI
            for (const auto& [id, obj] : ivtObjects ->requestedObjectInfos)
            {
                Button& button = tab.releaseObjectButtons[obj.id()];
                button.setLabel("Release");

                requestedGrid.add(Label(obj.dataset()), {row, 0}).add(Label(obj.className()), {row, 1})
                .add(button, {row, 2});
                row++;
            }

            requestedGroup.setLabel("Requested Objects");
            requestedGroup.addChild(requestedGrid);
        }

        GridLayout propertyGrid;
        row = 0;

        {
            std::vector<std::string> ids;

            for (const auto& [id, obj] : ivtObjects->requestedObjectInfos)
            {
                ids.push_back(obj.id().str());
            }
            std::string property = simox::alg::join(ids, ", ");

            tab.propertyLine.setValue(property);
            propertyGrid.add(Label("Property:"), {row, 0}).add(tab.propertyLine, {row, 1});
            row++;
        }

        VBoxLayout root = {grid, requestedGroup, propertyGrid, VSpacer()};

        RemoteGui_createTab(getName(), root, &tab);
    }

    void IVTObjectLocalizer::RemoteGui_update()
    {
        bool dbChanged = false;
        if (tab.objectDatasetComboBox.hasValueChanged())
        {
            std::scoped_lock lock(mutex);
            createRemoteGuiTab();
        }
        if (tab.requestObjectButton.wasClicked())
        {
            ObjectID id(tab.objectDatasetComboBox.getValue(), tab.objectNameComboBox.getValue());

            std::scoped_lock lock(mutex);

            if (ivtObjects->addObjects({id}))
            {
                ivtObjects->requestedObjectIDs.insert(id);

                dbChanged = true;

                createRemoteGuiTab();
            }
            else
            {
                ARMARX_INFO << "Object " << id << " already requested.";
            }

        }

        for (auto& [id, button] : tab.releaseObjectButtons)
        {
            if (button.wasClicked())
            {
                std::scoped_lock lock(mutex);

                dbChanged = true;

                // Release object from being localized by creating a new database with
                // the remaining requested IDs
                if (ivtObjects->releaseObjects({id}))
                {
                    if (ivtObjects->requestedObjectIDs.size() == 1)
                    {
                        ivtObjects->requestedObjectIDs.clear();
                    }
                    else
                    {
                        ivtObjects->requestedObjectIDs.erase(id);
                    }

                    createRemoteGuiTab();
                }
            }
        }


        if(dbChanged)
        {
            std::scoped_lock lock(requestedObjectsMutex);
            ivtCreateObjectDatabase();
            ivtInitializeCameraParameters();
        }

    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(IVTObjectLocalizer, IVTObjectLocalizer::GetDefaultName());
}  // namespace SimTrackArmarX::components::sim_track
