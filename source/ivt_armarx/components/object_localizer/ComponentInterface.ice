/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    ivtarmarx::object_localizer
 * author     Felizia Q ( felizia dot quetscher at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License



#pragma once


module ivtarmarx {  module components {  module object_localizer
{

    interface ComponentInterface
    {
    // Define your interface here.
    };

};};};
*/

#pragma once

#include <RobotAPI/interface/objectpose/ObjectPoseProvider.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>


module visionx
{
    interface IVTObjectLocalizerInterface extends
        armarx::objpose::ObjectPoseProvider,
        visionx::ImageProcessorInterface
    {
    };

};
