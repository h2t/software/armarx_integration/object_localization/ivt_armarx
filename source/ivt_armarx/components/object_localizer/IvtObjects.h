#pragma once

#include <vector>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>

#include <VisionX/interface/components/Calibration.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/core/ImageBuffer.h>

namespace armarx
{
    class IvtObjects : armarx::Logging
    {
        public:
            IvtObjects();
            ~IvtObjects();


            void setObjects(const std::vector<ObjectID>& objectIDs);
            bool addObjects(const std::vector<ObjectID>& objectIDs);
            bool releaseObjects(const std::vector<ObjectID>& objectIDs);
            bool updateObjects(const std::vector<ObjectID>& added, const std::vector<ObjectID>& released);

            std::vector<ObjectInfo> getSupportedObjects() const;
            std::vector<ObjectInfo> getRequestedObjects(std::vector<std::string> requestedNames) const;

        public:

            ObjectFinder objectFinder;

            std::map<ObjectID, ObjectInfo> requestedObjectInfos;
            std::set<ObjectID> requestedObjectIDs;

            std::mutex objectPosesMutex;
    };

}
