#include "IvtObjects.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/tools/opencv_conversions.h>

namespace armarx
{
    IvtObjects::IvtObjects()
    {
        armarx::Logging::setTag("IVT");
    }

    IvtObjects::~IvtObjects()
    {

    }

    void IvtObjects::setObjects(const std::vector<ObjectID>& objectIDs)
    {
        requestedObjectInfos.clear();
        addObjects(objectIDs);
    }

    bool IvtObjects::addObjects(const std::vector<ObjectID>& objectIDs)
    {
        return updateObjects(objectIDs, {});
    }

    bool IvtObjects::releaseObjects(const std::vector<ObjectID>& objectIDs)
    {
        return updateObjects({}, objectIDs);
    }

    bool IvtObjects::updateObjects(const std::vector<ObjectID>& added, const std::vector<ObjectID>& released)
    {
        if (added.empty() && released.empty())
        {
            return false;
        }

        bool changed = false;

        for (const ObjectID& id : released)
        {
            changed |= requestedObjectInfos.erase(id) > 0;
        }

        for (const auto& id : added)
        {
            if (requestedObjectInfos.count(id) == 0)
            {
                if (std::optional<ObjectInfo> info = objectFinder.findObject(id))
                {
                    ObjectInfo siftinfo(*info);
                    requestedObjectInfos.emplace(id, siftinfo);
                    changed = true;
                }

            }

        }

        return changed;
    }

    std::vector<ObjectInfo> IvtObjects::getSupportedObjects() const
    {
        bool checkPaths = false;
        std::vector<ObjectInfo> supportedObjects;
        std::vector<ObjectInfo> objects = objectFinder.findAllObjects(checkPaths);

        for (const ObjectInfo& object : objects)
        {
            if (std::filesystem::is_regular_file(object.file("dat").absolutePath))
            {
                supportedObjects.push_back(object);
            }

        }

        return supportedObjects;
    }

    std::vector<ObjectInfo> IvtObjects::getRequestedObjects(std::vector<std::string> requestedNames) const
    {
        std::vector<ObjectInfo> supportedObjects = IvtObjects::getSupportedObjects();
        std::vector<ObjectInfo> requestedObjects;
        if (requestedObjects.empty())
        {

            return supportedObjects;
        }

        for (auto& supportedObject: supportedObjects)
        {
           for(auto& requestedName : requestedNames)
           {
               if (requestedName == supportedObject.className())
               {
                   requestedObjects.push_back(supportedObject);
               }

           }

        }

        return requestedObjects;
    }

}
